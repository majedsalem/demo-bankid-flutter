import 'package:flutter/material.dart';
import 'package:flutter_appauth/flutter_appauth.dart';

class OpenIDConnectScreen extends StatefulWidget {
  @override
  _OpenIDConnectScreenState createState() => _OpenIDConnectScreenState();
}

class _OpenIDConnectScreenState extends State<OpenIDConnectScreen> {
  final FlutterAppAuth appAuth = FlutterAppAuth();
  String _accessToken = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('OpenID Connect'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: _login,
              child: Text('Login'),
            ),
            SizedBox(height: 20),
            Text('Access Token: $_accessToken'),
          ],
        ),
      ),
    );
  }

  Future<void> _login() async {
    try {
      final AuthorizationTokenResponse? result =
          await appAuth.authorizeAndExchangeCode(
        AuthorizationTokenRequest(
          'e25f8b15-490a-4260-9a74-8bac9b2963b6',
          'com.example.bankiddemo:/oauthredirect',
          discoveryUrl:
              'https://b659-83-250-46-25.eu.ngrok.io/.well-known/openid-configuration',
          scopes: ['openid', 'profile', 'personalidentitynumber'],
        ),
      );
      setState(() {
        _accessToken = result!.accessToken.toString();
      });
    } catch (e) {
      print('Error during OpenID Connect authentication: $e');
    }
  }
}
