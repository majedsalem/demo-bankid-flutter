import 'package:flutter/material.dart';
import 'package:flutter_appauth/flutter_appauth.dart';

class OpenIDConnectScreen extends StatefulWidget {
  @override
  _OpenIDConnectScreenState createState() => _OpenIDConnectScreenState();
}

class _OpenIDConnectScreenState extends State<OpenIDConnectScreen> {
  final FlutterAppAuth appAuth = FlutterAppAuth();
  String _accessToken = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('OpenID Connect'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: _login,
              child: Text('Login'),
            ),
            SizedBox(height: 20),
            Text('Access Token: $_accessToken'),
          ],
        ),
      ),
    );
  }

  Future<void> _login() async {
    try {
      final AuthorizationTokenResponse? result =
          await appAuth.authorizeAndExchangeCode(
        AuthorizationTokenRequest(
          'YOUR_CLIENT_ID',
          'YOUR_REDIRECT_URL',
          discoveryUrl: 'YOUR_DISCOVERY_URL',
          scopes: ['openid', 'profile', 'email'],
        ),
      );
      setState(() {
        _accessToken = result!.accessToken.toString();
      });
    } catch (e) {
      print('Error during OpenID Connect authentication: $e');
    }
  }
}
